vowels = ["a", "e", "i", "o", "u", "y"]
function isVowel(c) {
    return vowels.indexOf(c) >= 0
}

function findVowel(str){
    for (var i = 0; i < str.length; i++) {
        if (isVowel(str[i])) {
            return i;
    }
    }
}

function covfefe(s) {
    k = ["b", "c", "d", "f", "g", "k", "p", "s", "t", "v", "z"]
    v = ["p", "g", "t", "v", "k", "g", "b", "z", "d", "f", "s"]

    firstVowel = findVowel(s)
    if (firstVowel > s.length){ return s}

    stem = ''

    for (var i=0; i < s.length; i++){
        letter = s[i]
        if (i<=firstVowel) {
            stem += letter
        } else {
            if (isVowel(letter)) {
                stem += letter
            } else {
                stem += letter
                break
            }
        }
    }

    last = stem[stem.length - 1]
    rest = s.substring(stem.length)

    replacement = ''
    if (k.indexOf(last) >= 0){
        replacement = v[k.indexOf(last)]
    } else {
        replacement = last
    }

    nvi = findVowel(rest)
    nextVowel = nvi <= rest.length ? rest[nvi] : vowels[2]

    return stem + replacement + nextVowel + replacement + nextVowel
}

function eligible(s){
    return (s.length > 3) && (vowels.indexOf(s[0] < 0))
}

function covfefeSentence(src) {
    src = src.split(" ")
    result = new Array()
    for(var i=0; i < src.length; i++){
        word = src[i]
        if (eligible(word)){
            result.push(
                covfefe(word)
            )
        } else {
            result.push(word)
        }
    }
    return result
}

function update(){
    src = $("#human").val()
    result = covfefeSentence(src).join(' ')
    $("#trump").text(result)
}

function makeDelay(ms) {
    var timer = 0;
    return function(callback){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
};

$(function() {
    $("#human").change(function() {
        update
    })
    var delay = makeDelay(250);
    $("#human").keypress(function() {
        delay(update)
    })
});
